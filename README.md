A simple DOI to BibTex converter.

[Visit the working online page](https://doitex.gitlab.io/).

This converter uses static pages only and directly get's the data
from [crossref](https://www.crossref.org/). Thus you can simply copy/paste/fork
the files anyway and it works.

The API used is directly 'citeproc+json' skipping things that could go wrong
and also delivers author names in the 'surname, givenname' format which is more stable.
