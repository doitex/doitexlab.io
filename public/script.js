/*
| Document elements.
*/
let buttonCopy;
let buttonGet;
let divError;
let divResult;
let divResultWrap;
let divSpinner;
let divURL;
let input;
let spanClear;

/*
| Current request flying.
*/
let req;

/*
| Mapping json.type to bibtext type.
*/
const entryMap =
{
	['article-journal']: 'article',
	['BOOK']: 'book',
	['book-chapter']: 'incollection',
	['journal-article']: 'article',
	['monograph']: 'book',
	['proceedings-article']: 'inproceedings',
};

/*
| A set of url prefixes to be ignored in doi input.
*/
const doiPrefixes =
[
	'doi.org/',
	'dx.doi.org/',
	'http://doi.org/',
	'https://doi.org/',
	'http://dx.doi.org/',
	'https://dx.doi.org/',
];

/*
| Puts words in brackets that have more than one uppercase letters.
*/
function bracketCases( txt )
{
	const words = txt.split( ' ' );
	for( let a = 0, alen = words.length; a < alen; a++ )
	{
		let w = words[ a ];
		const nUpper = w.length - w.replace( /[A-Z]/g, '' ).length;
		if( nUpper > 1 ) words[ a ] = '{' + w + '}';
	}
	return words.join( ' ' );
}

/*
| Converts a bibtex object to string.
*/
function bibtex2str( bibtex )
{
	const keys = Object.keys( bibtex );
	const spaces = '             ';
	keys.sort( );
	let txt = '@' + bibtex.entrytype + '{ ' + bibtex.citationKey + ',\n';
	for( let key of keys )
	{
		if( key === 'entrytype' || key == 'citationKey' ) continue;
		txt +=
			'  '
			+ key
			+ spaces.substr( key.length )
			+ ' = {'
			+ bibtex[ key ]
			+ '},\n';
	}
	txt += '}';
	return txt;
}

/*
| Makes the citation key.
|
| ~json: json author data.
| ~year: already converted year (string).
*/
function citationKey( jauthor, year )
{
	let a1f = jauthor[ 0 ].family;

	// replaces common unicode chars with their closed ascii alike.
	a1f = a1f
		.replace( /Ä/g, 'A' )
		.replace( /Á/g, 'A' )
		.replace( /á/g, 'a' )
		.replace( /ä/g, 'a' )
		.replace( /ä/g, 'a' )
		.replace( /å/g, 'a' )
		.replace( /ã/g, 'a' )
		.replace( /č/g, 'c' )
		.replace( /ç/g, 'c' )
		.replace( /Ç/g, 'C' )
		.replace( /ć/g, 'c' )
		.replace( /É/g, 'E' )
		.replace( /è/g, 'e' )
		.replace( /é/g, 'e' )
		.replace( /ě/g, 'e' )
		.replace( /ḡ/g, 'g' )
		.replace( /í/g, 'i' )
		.replace( /î/g, 'i' )
		.replace( /ı/g, 'i' )
		.replace( /ñ/g, 'n' )
		.replace( /ü/g, 'u' )
		.replace( /Ü/g, 'U' )
		.replace( /š/g, 's' )
		.replace( /ş/g, 's' )
		.replace( /ţ/g, 't' )
		.replace( /ö/g, 'o' )
		.replace( /ó/g, 'o' )
		.replace( /Ö/g, 'O' )
		.replace( /ő/g, 'o' )
		.replace( /Ø/g, 'O' )
		.replace( /ø/g, 'o' )
		.replace( /®/g, 'R' )
		.replace( /ü/g, 'u' )
		.replace( /Ü/g, 'U' )
		.replace( /ú/g, 'u' )
		.replace( /ž/g, 'z' )
		.replace( /Ž/g, 'Z' )
		.replace( /’/g, '' )
		.replace( /&/g, '' )
		.replace( /\^/g, '' );

	return fixUpperCase( a1f ) + '_' + year;
}

/*
| Converts a json text to BibTeX.
*/
function convText( txt )
{
	if( txt === '' ) return;
	txt = specialChars( txt );
	return bracketCases( txt );
}

/*
| Convers a number.
*/
function convNumber( txt )
{
	if( !txt ) return;
	while( txt.startsWith( '0' ) ) txt = txt.substr( 1 );
	if( txt === '-1' ) return;
	if( txt === '' ) return;
	return txt;
}

/*
| Converts an URL.
*/
function convURL( txt )
{
	txt = txt.replace( /^http:\/\//, 'https://' );
	txt = txt.replace( /^https:\/\/dx.doi.org\//, 'https://doi.org/' );
	return txt;
}

/*
| Converts a json reply to bibtex.
*/
function doi2bib( json )
{
	const entrytype = entryMap[ json.type ];
	const author = people( json.author );
	const doi = json.DOI;
	const editor = people( json.editor );
	const url = convURL( json.URL );
	const date = json[ 'published-print' ] || json.published || json.issued;
	const year = date[ 'date-parts' ][ 0 ][ 0 ] + '';
	const title = convText( json.title );
	let pages = json.page;
	let volume = json.volume;
	let number = convNumber( json.issue );

	const bibtex = { };
	bibtex.citationKey = citationKey( json.author, year );

	const publisher = json.publisher;
	if( publisher === 'arXiv' )
	{
		bibtex.entrytype = 'techreport';
		bibtex.type = 'Preprint on ArXiv';
		const id = json.id;
		if( id )
		{
			const ioa = id.indexOf( 'arxiv.' );
			if( ioa >= 0 ) bibtex.number = id.substr( ioa + 6 );
		}
	}
	else if( entrytype && entrytype !== '' ) bibtex.entrytype = entrytype;

	if( author && author !== '' ) bibtex.author = author;
	if( year && year !== '' ) bibtex.year = year;
	if( doi && doi !== '' ) bibtex.doi = doi;
	if( editor && editor !== '' ) bibtex.editor = editor;
	let booktitle, journal;
	switch( entrytype )
	{
		case 'article':
			journal = convText( json['container-title' ] );
			break;
		case 'inproceedings':
		case 'incollection':
			booktitle = convText( json['container-title' ] );
			break;
	}
	if( title && title !== '' ) bibtex.title = title;
	if( booktitle && booktitle !== '' ) bibtex.booktitle = booktitle;
	if( journal && journal !== '' ) bibtex.journal = journal;
	if( url && url !== '' ) bibtex.url = url;
	if( number && number !== '' ) bibtex.number = number;
	if( pages && pages !== '' )
	{
		// replaces unicode hyphen (U+2010) with "minus"
		pages = pages.replace( '‐', '-' );
		// replaces minus with --, BibTeX hyphen
		pages = pages.replace( /([^-])-([^-])/g, '$1--$2' );
		bibtex.pages = pages;
	}
	if( volume )
	{
		if( volume.startsWith( 'Vol ' ) ) volume = volume.substr( 4 );
		while( volume.startsWith( '0' ) ) volume = volume.substr( 1 );
		volume = convText( volume );
		if( volume && volume !== '' ) bibtex.volume = volume;
	}
	return bibtex;
}

/*
| Fixes for all uppercase text.
*/
function fixUpperCase( txt )
{
	if( txt !== txt.toUpperCase( ) ) return txt;
	else return txt[ 0 ] + txt.substr( 1 ).toLowerCase( );
}

/*
| Gets the input value.
*/
function getInput( )
{
	let v = input.value;
	v = v.trim( );
	for( let p of doiPrefixes )
	{
		if( v.startsWith( p ) ) return v.substr( p.length );
	}
	return v;
}

/*
| Input has changed.
*/
function inputChange( )
{
	const v = input.value;
	const gv = getInput( );
	buttonGet.disabled = gv.substr( 0, 3 ) !== '10.';
	spanClear.style.display = v === '' ? 'none' : 'block';
}

/*
| Makes a new crossref request
*/
function newReq( doi )
{
	divError.style.display = 'none';
	divURL.style.display = 'none';

	if( req )
	{
		req.abort( );
		divSpinner.style.display = 'none';
	}

	req = new XMLHttpRequest( );
	const url = 'https://doi.org/' + getInput( );
	req.open( 'GET', url, true );
	req.setRequestHeader( 'Accept', 'application/citeproc+json' );
	req.onload = reqLoad;
	req.onerror = reqError;
	req.send( null );
	divResultWrap.style.display = 'none';
	divSpinner.style.display = 'block';
}

/*
| Parses people from author/editor fields.
*/
function people( json )
{
	if( !json ) return;
	let result = '';
	for( let e of json )
	{
		let family = e.family;
		let given = e.given;
		if( !family ) continue;
		if( result !== '' ) result += ' and ';

		family = fixUpperCase( family );

		result += specialChars( family );
		if( given )
		{
			given = fixUpperCase( given );
			result += ', ';
			result += specialChars( given );
		}
	}
	return result;
}

/*
| Request completed (onload event)
*/
function reqLoad( )
{
	divSpinner.style.display = 'none';
	let json;
	try{ json = JSON.parse( req.responseText ); }
	catch( _ )
	{
		console.log( 'response:', req.responseText );
		showError( req.responseText );
		req = undefined;
		return;
	}
	console.log( 'response:', json );
	req = undefined;
	const bibtex = doi2bib( json );
	const lastResult = divResult.innerHTML = bibtex2str( bibtex );

	buttonCopy.onclick = ( ) =>
		{ navigator.clipboard.writeText( lastResult ); };

	divResultWrap.style.display = 'flex';
	if( bibtex.url )
	{
		divURL.innerHTML = '<a href="' + bibtex.url + '">' + bibtex.url + '</a>';
		divURL.style.display = 'block';
	}
}

/*
| Request got an error.
*/
function reqError( )
{
	divSpinner.style.display = 'none';
	showError( req.statusText );
	req = undefined;
}

/*
| Displays an error message.
*/
function showError( message )
{
	divError.innerText = message;
	divError.style.display = 'block';
}

/*
| Replaces some Unicode with LaTex codes.
*/
function specialChars( txt )
{
	if( !txt ) return;
	return(
		txt
		.replace( /Ä/g, '{\\"A}' )
		.replace( /Á/g, '{\\\'A}' )
		.replace( /á/g, '{\\\'a}' )
		.replace( /ä/g, '{\\"a}' )
		.replace( /ä/g, '{\\"a}' )
		.replace( /å/g, '{\\aa}' )
		.replace( /ã/g, '{\\~{a}}' )
		.replace( /č/g, '{\\v{c}}' )
		.replace( /ç/g, '{\\c{c}}' )
		.replace( /Ç/g, '{\\c{C}}' )
		.replace( /ć/g, '{\\\'c}' )
		.replace( /É/g, '{\\\'E}' )
		.replace( /è/g, '{\\`e}' )
		.replace( /é/g, '{\\\'e}' )
		.replace( /ě/g, '{\\v{e}}' )
		.replace( /ḡ/g, '{\\=g}' )
		.replace( /í/g, '{\\\'i}' )
		.replace( /î/g, '{\\^i}' )
		.replace( /ı/g, '{\\i}' )
		.replace( /ñ/g, '{\\~{n}}' )
		.replace( /ü/g, '{\\"u}' )
		.replace( /Ü/g, '{\\"U}' )
		.replace( /š/g, '{\\v{s}}' )
		.replace( /ş/g, '{\\c{s}}' )
		.replace( /ţ/g, '{\\c{t}}' )
		.replace( /ö/g, '{\\"o}' )
		.replace( /ó/g, '{\\\'o}' )
		.replace( /Ö/g, '{\\"O}' )
		.replace( /ő/g, '{\\H{o}}' )
		.replace( /Ø/g, '{\\O}' )
		.replace( /ø/g, '{\\o}' )
		.replace( /®/g, '{\\textregistered}' )
		.replace( /ü/g, '{\\"u}' )
		.replace( /Ü/g, '{\\"U}' )
		.replace( /ú/g, '{\\\'u}' )
		.replace( /ž/g, '{\\v{z}}' )
		.replace( /Ž/g, '{\\v{Z}}' )
		.replace( /’/g, '\'' )
		.replace( /&/g, '{\\&}' )
	);
}

window.onload = ( ) =>
{
	buttonCopy = document.getElementById( 'copy' );
	buttonGet = document.getElementById( 'get' );
	divError = document.getElementById( 'error' );
	divResult = document.getElementById( 'result' );
	divResultWrap = document.getElementById( 'resultWrap' );
	divSpinner = document.getElementById( 'spinner' );
	divURL = document.getElementById( 'url' );
	input = document.getElementById( 'input' );
	spanClear = document.getElementById( 'clear' );

	input.addEventListener( 'keydown', ( e ) => e.key === 'Enter' && newReq( ) );
	input.addEventListener( 'input', inputChange );
	buttonGet.addEventListener( 'click', newReq );
	spanClear.addEventListener( 'click', ( ) => {
		input.value = '';
		input.focus( );
		inputChange( );
	} );
	// some browsers fill stuff out on load
	inputChange( );
};
